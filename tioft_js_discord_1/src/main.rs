#![recursion_limit="500"]
#![warn(non_upper_case_globals)]

#[macro_use]
extern crate stdweb;

const PREFIX: &'static str = "@@@@";
const token: &'static str = "NDU3MjIyNjUxNzczOTc2NTk3.Duj3yw.1wXT7-BLaV8Zhpfod2enCMPhB_Q";

fn main() {
    stdweb::initialize();
    let handle_message = |client: stdweb::Object ,msg: stdweb::Object, msg_content: String|{
        if (&msg_content).to_lowercase().trim().contains("test0"){
            js! {@{&msg}.reply("t0 done");};
        }
        if is_command(&msg_content, "say") {
            let message: Vec<&str> = msg_content.trim().split_whitespace().collect();
            let message: String = String::from(message[1..].join(" "));
            say(&msg, message.as_str());
        }

        if is_command(&msg_content, "ping") {
            say(&msg, "pong");
            js! {@{&msg}.channel.send(@{&client}.ping + " ms");};
        }
    };

    js!(
        const client = new Discord.Client();
        var handle_message = @{handle_message};

        client.on("ready", () => {
          console.log("Logged in as " + client.user.tag + " !");
        });

        function display_map_attachments(value, key, map) {
            var vpurl = value.proxyURL;
            document.body.innerHTML += "<a href='" + vpurl + "'>" + "&nbsp;&nbsp;&nbsp;&nbsp;" +vpurl +"</a><br />";
        };

        function display_it(msg){
          var unf = msg.author.tag;
          var msgc = msg.content;
          var channel = msg.channel;
          var guild_name = channel.guild.name;
          document.body.innerHTML += "["+ guild_name + " - " + channel.name +"] "+ unf + " : " + msgc + " <br />";
          if (msg.attachments.size != 0){
              document.body.innerHTML += "&nbsp;&nbsp;&nbsp;[Attachments] <br />";
              msg.attachments.forEach(display_map_attachments);
          };
      };


        client.on("message", msg => {
          console.log(msg);
          display_it(msg);
          handle_message(client,msg,msg.content);
        });

        client.login( @{token} );
    );

    stdweb::event_loop();
}

fn is_command(message: &String, command_name: &str) -> bool {
    // message.trim() == String::from(PREFIX) + command_name
    let message: Vec<&str> = message.trim().split_whitespace().collect();
    if (message[0] == String::from(PREFIX) + command_name) {
        true
    }else{
        false
    }
}

fn say_reply(msg: &stdweb::Object, content: &str){
    js!{@{msg}.reply(@{content})};
}

fn say(msg: &stdweb::Object, content: &str){
    js!{@{msg}.channel.send(@{content})};
}
